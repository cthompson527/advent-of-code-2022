use std::cmp;

fn get_sums(input: Vec<String>) -> Vec<i32> {
    let mut v = vec![];
    let mut sum = 0;

    for val in input {
        if val == "" {
            v.push(sum);
            sum = 0;
        } else {
            let num: i32 = val.parse().unwrap();
            sum += num;
        }
    }

    v.push(sum);
    v
}

pub fn part_one(input: Vec<String>) -> i32 {
    let mut maximum = 0;
    for val in get_sums(input) {
        maximum = cmp::max(maximum, val)
    }
    maximum
}

pub fn part_two(input: Vec<String>) -> i32 {
    let mut vals = get_sums(input);
    vals.sort_by(|a, b| b.cmp(a));
    vals[0] + vals[1] + vals[2]
}


#[cfg(test)]
mod tests {
    use super::get_sums;
    use super::part_one;
    use super::part_two;

    fn sample_input() -> Vec<String> {
         vec![
             "1000".to_string(),
             "2000".to_string(),
             "3000".to_string(),
             "".to_string(),
             "4000".to_string(),
             "".to_string(),
             "5000".to_string(),
             "6000".to_string(),
             "".to_string(),
             "7000".to_string(),
             "8000".to_string(),
             "9000".to_string(),
             "".to_string(),
             "10000".to_string()
         ]
    }

    #[test]
    fn test() {
        assert_eq!(
            get_sums(sample_input()),
            vec![6000, 4000, 11000, 24000, 10000]
        );
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 24000);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 45000);
    }
}
