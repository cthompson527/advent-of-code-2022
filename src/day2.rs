#[derive(PartialEq, Debug, Clone)]
enum PICK {
    ROCK,
    PAPER,
    SCISSORS,
}

#[derive(PartialEq, Debug, Clone)]
enum OUTCOME {
    WIN,
    LOSE,
    DRAW,
}

fn get_picks(input: &String) -> (PICK, PICK) {
    let v: Vec<&str> = input.split(' ').collect();
    let your_pick = match v[0] {
        "A" => PICK::ROCK,
        "B" => PICK::PAPER,
        "C" => PICK::SCISSORS,
        _   => panic!("Invalid input")

    };
    let my_pick = match v[1] {
        "X" => PICK::ROCK,
        "Y" => PICK::PAPER,
        "Z" => PICK::SCISSORS,
        _   => panic!("Invalid input")

    };
    (your_pick, my_pick)
}

fn get_picks_part_two(input: &String) -> (PICK, OUTCOME) {
    let v: Vec<&str> = input.split(' ').collect();
    let your_pick = match v[0] {
        "A" => PICK::ROCK,
        "B" => PICK::PAPER,
        "C" => PICK::SCISSORS,
        _   => panic!("Invalid input")

    };
    let outcome = match v[1] {
        "X" => OUTCOME::LOSE,
        "Y" => OUTCOME::DRAW,
        "Z" => OUTCOME::WIN,
        _   => panic!("Invalid input")

    };
    (your_pick, outcome)
}

fn get_outcome(selections: (PICK, PICK)) -> OUTCOME {
    let your_pick = selections.0;
    let my_pick = selections.1;

    match your_pick {
        PICK::ROCK => match my_pick {
            PICK::ROCK => OUTCOME::DRAW,
            PICK::PAPER => OUTCOME::WIN,
            PICK::SCISSORS => OUTCOME::LOSE,
        },
        PICK::PAPER => match my_pick {
            PICK::ROCK => OUTCOME::LOSE,
            PICK::PAPER => OUTCOME::DRAW,
            PICK::SCISSORS => OUTCOME::WIN,
        },
        PICK::SCISSORS => match my_pick {
            PICK::ROCK => OUTCOME::WIN,
            PICK::PAPER => OUTCOME::LOSE,
            PICK::SCISSORS => OUTCOME::DRAW,
        },
    }
}

fn find_selection(your_pick: PICK, outcome: OUTCOME) -> PICK {
    match your_pick {
        PICK::ROCK => match outcome {
            OUTCOME::DRAW => PICK::ROCK,
            OUTCOME::WIN => PICK::PAPER,
            OUTCOME::LOSE => PICK::SCISSORS,
        },
        PICK::PAPER => match outcome {
            OUTCOME::DRAW => PICK::PAPER,
            OUTCOME::WIN => PICK::SCISSORS,
            OUTCOME::LOSE => PICK::ROCK,
        },
        PICK::SCISSORS => match outcome {
            OUTCOME::DRAW => PICK::SCISSORS,
            OUTCOME::WIN => PICK::ROCK,
            OUTCOME::LOSE => PICK::PAPER,
        },
    }
}

pub fn part_one(input: Vec<String>) -> i32 {
    let mut total = 0;
    for val in &input {
        let (your_pick, my_pick) = get_picks(&val).clone();
        total += match get_outcome((your_pick, my_pick.clone())) {
            OUTCOME::WIN  => 6,
            OUTCOME::LOSE => 0,
            OUTCOME::DRAW => 3,
        };

        total += match my_pick {
            PICK::ROCK => 1,
            PICK::PAPER => 2,
            PICK::SCISSORS => 3,
        };
    }
    total
}

pub fn part_two(input: Vec<String>) -> i32 {
    let mut total = 0;
    for val in &input {
        let (your_pick, outcome) = get_picks_part_two(&val).clone();
        let my_pick = find_selection(your_pick, outcome.clone());
        total += match outcome {
            OUTCOME::WIN  => 6,
            OUTCOME::LOSE => 0,
            OUTCOME::DRAW => 3,
        };

        total += match my_pick {
            PICK::ROCK => 1,
            PICK::PAPER => 2,
            PICK::SCISSORS => 3,
        };
    }
    total
}


#[cfg(test)]
mod tests {
    use super::get_picks;
    use super::get_outcome;
    use super::find_selection;
    use super::get_picks_part_two;
    use super::part_one;
    use super::part_two;
    use super::PICK;
    use super::OUTCOME;

    fn sample_input() -> Vec<String> {
         vec![
             "A Y".to_string(),
             "B X".to_string(),
             "C Z".to_string(),
         ]
    }

    #[test]
    fn test() {
        let input = sample_input();
        assert_eq!(get_picks(&input[0]), (PICK::ROCK, PICK::PAPER));
        assert_eq!(get_picks(&input[1]), (PICK::PAPER, PICK::ROCK));
        assert_eq!(get_picks(&input[2]), (PICK::SCISSORS, PICK::SCISSORS));
    }

    #[test]
    fn test_my_outcome() {
        assert_eq!(get_outcome((PICK::ROCK, PICK::ROCK)), OUTCOME::DRAW);
        assert_eq!(get_outcome((PICK::ROCK, PICK::PAPER)), OUTCOME::WIN);
        assert_eq!(get_outcome((PICK::ROCK, PICK::SCISSORS)), OUTCOME::LOSE);
        assert_eq!(get_outcome((PICK::PAPER, PICK::ROCK)), OUTCOME::LOSE);
        assert_eq!(get_outcome((PICK::PAPER, PICK::PAPER)), OUTCOME::DRAW);
        assert_eq!(get_outcome((PICK::PAPER, PICK::SCISSORS)), OUTCOME::WIN);
        assert_eq!(get_outcome((PICK::SCISSORS, PICK::ROCK)), OUTCOME::WIN);
        assert_eq!(get_outcome((PICK::SCISSORS, PICK::PAPER)), OUTCOME::LOSE);
        assert_eq!(get_outcome((PICK::SCISSORS, PICK::SCISSORS)), OUTCOME::DRAW);
    }

    #[test]
    fn test_get_picks_part_two() {
        let input = sample_input();
        assert_eq!(get_picks_part_two(&input[0]), (PICK::ROCK, OUTCOME::DRAW));
        assert_eq!(get_picks_part_two(&input[1]), (PICK::PAPER, OUTCOME::LOSE));
        assert_eq!(get_picks_part_two(&input[2]), (PICK::SCISSORS, OUTCOME::WIN));
    }


    #[test]
    fn test_find_selection() {
        assert_eq!(find_selection(PICK::ROCK,     OUTCOME::DRAW), PICK::ROCK);
        assert_eq!(find_selection(PICK::ROCK,     OUTCOME::WIN),  PICK::PAPER);
        assert_eq!(find_selection(PICK::ROCK,     OUTCOME::LOSE), PICK::SCISSORS);
        assert_eq!(find_selection(PICK::PAPER,    OUTCOME::DRAW), PICK::PAPER);
        assert_eq!(find_selection(PICK::PAPER,    OUTCOME::WIN),  PICK::SCISSORS);
        assert_eq!(find_selection(PICK::PAPER,    OUTCOME::LOSE), PICK::ROCK);
        assert_eq!(find_selection(PICK::SCISSORS, OUTCOME::DRAW), PICK::SCISSORS);
        assert_eq!(find_selection(PICK::SCISSORS, OUTCOME::WIN),  PICK::ROCK);
        assert_eq!(find_selection(PICK::SCISSORS, OUTCOME::LOSE), PICK::PAPER);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 15);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 12);
    }
}
