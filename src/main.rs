use std::process::exit;
use structopt::StructOpt;

mod day1;
mod day2;

mod readfile;

#[derive(Debug, StructOpt)]
#[structopt(name = "AoC2022", about = "The answers to my Advent of Code")]
struct Cli {
    #[structopt(short, long)]
    day: i32,

}

fn input<T>(day: i32) -> Vec<T>
where T: std::str::FromStr, <T as std::str::FromStr>::Err : std::fmt::Debug
{
    let filename = format!("inputs/input{}.txt", day);
    readfile::read::<T>(filename)
}

fn main() {
    let args = Cli::from_args();
    let (p_one_ans, p_two_ans): (i64, i64) = match args.day {
        1 => {
            let i = input::<String>(1);
            (day1::part_one(i.clone()) as i64, day1::part_two(i) as i64)
        },
        2 => {
            let i = input::<String>(2);
            (day2::part_one(i.clone()) as i64, day2::part_two(i) as i64)
        },
        _ => {
            println!("Invalid day input: {}", args.day);
            exit(-1);
        }
    };
    println!("The answer for day {} - part 1 is: {}", args.day, p_one_ans);
    println!("The answer for day {} - part 2 is: {}", args.day, p_two_ans);
}
