use std::fs::File;
use std::io::{BufRead, BufReader};

pub(crate) fn read<T>(filename: String) -> Vec<T>
where T: std::str::FromStr, <T as std::str::FromStr>::Err : std::fmt::Debug
{
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    let mut r = vec![];

    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        r.push(line.trim().parse::<T>().unwrap())
    }
    r
}

#[cfg(test)]
mod tests {
    use super::read;

    #[test]
    fn test_parse_ints() {
        assert_eq!(
            read::<i32>("inputs/read_file_test.txt".to_string()),
            vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        );
    }

    #[test]
    fn test_parse_strings() {
        assert_eq!(
            read::<String>("inputs/read_file_test.txt".to_string()),
            vec!["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        );
    }
}

